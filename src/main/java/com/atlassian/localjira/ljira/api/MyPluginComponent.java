package com.atlassian.localjira.ljira.api;

public interface MyPluginComponent
{
    String getName();
}