const colorInput = document.getElementById('color-input')

colorInput.addEventListener('change', setColor)

const setColor = function () {
    colorInput.style.color = colorInput.value
    console.log(colorInput.value)
}