package ut.com.atlassian.localjira.ljira;

import org.junit.Test;
import com.atlassian.localjira.ljira.api.MyPluginComponent;
import com.atlassian.localjira.ljira.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}